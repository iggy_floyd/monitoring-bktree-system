/* 
 * File:   Monitor.hpp
 * Author: Igor Marfin <Unister Gmbh 2014 > igor.marfin@unister.de
 *
 * Created on July 6, 2015, 7:55 PM
 */

#ifndef MONITOR_HPP
#define	MONITOR_HPP

#include <iostream>
#include <cmw-cmx-cpp/ProcessComponent.h>
#include <cmw-cmx-cpp/Registry.h>
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/CmxSupport.h>
#include <exception>
#include <unistd.h>
#include <string>

class Monitor
{
public:
    Monitor( const char * nameValue,std::string _value) {
       cmw::cmx::ProcessComponent::update();

       cmw::cmx::ImmutableComponent::iterator itComp = Monitor::component->begin(); 
       for (; itComp != Monitor::component->end(); ++itComp)
       {
           try{
               cmw::cmx::CmxRef m = *itComp;
               if (std::string(nameValue).compare(m.name()) == 0) {
                  
                   Monitor::component->remove(m);
                   break;
               }
               
           }  catch (cmw::cmx::CmxException & e)
           {
               
           }
       }
       
       cmw::cmx::CmxString value= Monitor::component->newString(nameValue,256); 
       value = _value;
  
        cmw::cmx::ProcessComponent::update();
  
       
    }
    
  
    ~Monitor() {  }

    static cmw::cmx::ComponentPtr component;
    static void initialize(const char * nameComponent);
  
    
};

#endif	/* MONITOR_HPP */

