#!/usr/bin/env bash


#@author @2014 Igor Marfin [Unister Gmbh] <igor.marfin@unister.de>
#@description  Monitor of the resources


# Logging service initialization
LOG_TAG=MONITOR_RESOURCES;
export LOG_FILE="$cayley_bktree_logging_path";

if  [ -z  $BL_LEVEL  ]
then

	logsdir=$(pwd)/../logs;
	cd $logsdir;
	. bashlog;
	cd - 2>&1 /dev/null;
fi




# define 'return' codes of different subroutines
declare -A CODES;
CODES[OK]=0;
CODES[ERROR]=1;



#main function
main(){
# reading settings from config.ini
source <(grep = config.ini |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\2/'" | sh);

local _BACKUP_MONITOR_SERVICE="$BACKUP_MONITOR_SERVICE";
local NAME_COMPONENT="$2";
# lockfiles: for writeing
    if [ -z "$_BACKUP_MONITOR_SERVICE" ]		
	then
		_BACKUP_MONITOR_SERVICE="${cayley_bktree_backup_monitor}";
	fi


    # infinite loop

    # an error code to be returned
    local _err=${CODES[OK]};
    while [ true ]
    do
	[[ -z "$1" ]] && continue;
	[[ -z "$NAME_COMPONENT" ]] &&  NAME_COMPONENT="RESOURCE_MONITOR"

       local _resources=($(echo `ps aux | grep -v grep | grep -v "monitor_resources.sh" | grep -v "respawn_double_fork.sh" | grep "$1"`));

       [[ -z "$_resources" ]] && continue;
       _resources=${_resources[*]:0:12}; # take first 12 elements of the obtained array
	$_BACKUP_MONITOR_SERVICE --wait 200000 --nameComponent="$NAME_COMPONENT" --nameValue=PID --value=${_resources[1]}  1&>2  2> /dev/null &
	$_BACKUP_MONITOR_SERVICE --wait 200000 --nameComponent="$NAME_COMPONENT" --nameValue=VSZ --value=${_resources[4]}  1&>2  2> /dev/null &
	$_BACKUP_MONITOR_SERVICE --wait 200000 --nameComponent="$NAME_COMPONENT" --nameValue=RSS --value=${_resources[5]}  1&>2  2> /dev/null &
        sleep 0.2
    done


_bashlog INFO 'main() has stoped....';
return ${CODES[OK]};
}


main "$1" "$2";
