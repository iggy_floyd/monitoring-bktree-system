#!/usr/bin/env bash


#@author @2014 Igor Marfin [Unister Gmbh] <igor.marfin@unister.de>
#@description  Backup rotation system

# reading settings from config.ini
source <(grep = config.ini |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\2/'" | sh);

# Logging service initialization
LOG_TAG=BACKUP_ROTATION_SYSTEM;
export LOG_FILE="$cayley_bktree_logging_path";

if  [ -z  $BL_LEVEL  ]
then

	logsdir=$(pwd)/../logs;
	cd $logsdir;
#	. config.sh;
	. bashlog;
	cd - 2>&1 /dev/null;
fi




# to store settings of the backup
declare -A settings; # please note, that associative array can be created in the bash with version >4.0

# to store modification times of the dumps
declare -A backupList; # please note, that associative array can be created in the bash with version >4.0

# define 'return' codes of different subroutines
declare -A CODES;
CODES[OK]=0;
CODES[ERROR]=1;
CODES[NO_BACKUP_FILES]=101;
CODES[BACKUP_IS_FORBIDDEN]=102;


# sets up of the backup settings
backup_settings() {

    _bashlog INFO 'Setting up parametes starts...';
	# SOURCE dir 
	if [ -n "$DUMP_DIR_BKTREE" ]
		then
			settings[SOURCE]="${DUMP_DIR_BKTREE}";
		else
			settings[SOURCE]="${cayley_bktree_dump_dirname}";
	fi

	# DESTINATION dir 
	if [ -n "$BACKUP_DIR_BKTREE" ]
		then 
			settings[DESTINATION]="${BACKUP_DIR_BKTREE}";
		else
			settings[DESTINATION]="${cayley_bktree_backup_dirname}";
	fi

	# DESTINATION dir
	if [ -n "$CRASHED_DIR_BKTREE" ]
		then
			settings[CRASHED]="${CRASHED_DIR_BKTREE}";
		else
			settings[CRASHED]="${cayley_bktree_crashed_dirname}";
	fi


	# extenstion of the  dumps 
	settings[EXTENSION]=".db";

	# datetime format in the dirnames of backup (we use the datetime up-to seconds!)
	settings[DATETIMEFORMAT]="+%Y_%m_%d_%H_%M_%S";


    # lockfiles: for reading
    if [ -n "$BACKUP_LOCKFILE_READ_BKTREE" ]
		then
			settings[LOCKFILE_READ]="${BACKUP_LOCKFILE_READ_BKTREE}";
		else
			settings[LOCKFILE_READ]="${cayley_bktree_backup_lockfile_read}";

	fi

    # lockfiles: for writeing
    if [ -n "$BACKUP_LOCKFILE_WRITE_BKTREE" ]
		then
			settings[LOCKFILE_WRITE]="${BACKUP_LOCKFILE_WRITE_BKTREE}";
		else
			settings[LOCKFILE_WRITE]="${cayley_bktree_backup_lockfile_write}";

	fi


    # lockfiles: for writeing
    if [ -n "$BACKUP_MONITOR_SERVICE" ]
		then
			settings[BACKUP_MONITOR_SERVICE]="${BACKUP_MONITOR_SERVICE}";
		else
			settings[BACKUP_MONITOR_SERVICE]="${cayley_bktree_backup_monitor}";

	fi

    #  period: in which periods to make backup
    settings[BACKUP_PERIOD]=1; # in seconds


    #  period: in which periods to make backup
    settings[STORE_BACKUP]=10; #the latest 10 backups will be stored

    _bashlog INFO 'Setting up parametes has ended';
    return ${CODES[OK]};

}

# makes an initialization
initialization(){
    _bashlog INFO 'Initialization starts...';

	if [ -z "${settings[SOURCE]}" ]
	    then
		    _bashlog CRIT "SOURCE is not defined"  ;exit ${CODES[ERROR]};
	    fi

    [[ -n "${settings[DESTINATION]}" ]] && mkdir -p ${settings[DESTINATION]} 2> /dev/null ||
    (  _bashlog CRIT "DESTINATION is not defined");
    [[ -z "${settings[DESTINATION]}" ]] && exit ${CODES[ERROR]};


    [[ -n "${settings[CRASHED]}" ]] && mkdir -p ${settings[CRASHED]} 2> /dev/null ||
    (  _bashlog CRIT "CRASHED is not defined");
    [[ -z "${settings[CRASHED]}" ]] && exit ${CODES[ERROR]};


    _bashlog INFO 'Initialization has ended';
    return ${CODES[OK]};
}

# does backup of a particular dump
# returns: CODES[OK] if we have backuped, CODES[ERROR] otherwise
backup_file() {
    # stores the argument (a file to be backuped)
    local _file_to_be_backuped="$1";
    local _destination_dir="$2";
    local _crashed="$3";

    # should we mkdir of the distination backup folder and backup a file to it?
    local _do_backup=0;

    if [ -z "$_file_to_be_backuped" ]
	    then
		    _bashlog CRIT "[backup_file()]... no file to be backuped is given"  ;return  ${CODES[ERROR]};
	    fi

    if [ -z "$_destination_dir" ]
	    then
		    _bashlog CRIT "[backup_file()]... no distination directory is given"  ;return  ${CODES[ERROR]};
	    fi


    # copy dumps after the crashing system and returns back
    if [ -n "${_crashed}"  ]
        then
            [[ ! -d  $"_destination_dir"   ]] && mkdir -p  ${_destination_dir};
            cp ${_file_to_be_backuped}  ${_destination_dir};
            return ${CODES[OK]};
    fi

    local _file_to_be_backuped_basename=`basename $_file_to_be_backuped`;

    # check the modification time of the file to be backuped and decide about backing-up
    if [ -z ${backupList[$_file_to_be_backuped_basename]} ]
        then
            # we make a backup of the file first time.
            # save its last modification time in epochs
            backupList[$_file_to_be_backuped_basename]=`stat -c %Y $_file_to_be_backuped`;
            _do_backup=1;

        else
            #ckeck if the file was updated since last backup
            if [ `stat -c %Y $_file_to_be_backuped` -gt ${backupList[$_file_to_be_backuped_basename]}  ]
                  then
                    # store its modification time in epochs
                    backupList[$_file_to_be_backuped_basename]=`stat -c %Y $_file_to_be_backuped`;
                    _do_backup=1;
            fi
    fi

    #backup goes here
    if [ $_do_backup -gt 0 ]
        then
            [[ ! -d  $"_destination_dir"   ]] && mkdir -p  ${_destination_dir};
            cp ${_file_to_be_backuped}  ${_destination_dir};
            return ${CODES[OK]};
    fi

    return ${CODES[ERROR]};
}


# does backup of all dumps
# returns: either CODES[NO_BACKUP_FILES] or CODES[BACKUP_IS_FORBIDDEN], or CODES[ERROR], or CODES[OK]
backup() {


    # stores absolute paths to dumps to be backuped
    local _files_to_backup=`ls ${settings[SOURCE]}/* | grep ${settings[EXTENSION]}`;

    local _crashed="$1";

    # keeps the code error of the backup_file
    local _err=${CODES[OK]};

    # if nothing is found, return
    [[ -z "${_files_to_backup}" ]] && return  ${CODES[NO_BACKUP_FILES]};

    # check if the backuping is allowed
    if [   -f "${settings[SOURCE]}/${settings[LOCKFILE_READ]}" ]
        then
            # we are not allowed to backup: bk-tree currently dumps trees to files
            echo "debug:: BACKUP_IS_FORBIDDEN";
            _bashlog INFO "debug:: BACKUP_IS_FORBIDDEN";
            return ${CODES[BACKUP_IS_FORBIDDEN]};
        else # lock the dumped directory: we forbid the bk-tree to dump to the directory
            touch  ${settings[SOURCE]}/${settings[LOCKFILE_WRITE]};
        fi

    local _destination_dir='';

    if [ -z "${_crashed}" ]
        then
            _destination_dir=${settings[DESTINATION]}/`date ${settings[DATETIMEFORMAT]}`;
        else
            _destination_dir=${settings[CRASHED]}/`date ${settings[DATETIMEFORMAT]}`;
    fi



    for _file in $_files_to_backup
        do
             backup_file "$_file" "$_destination_dir" $_crashed;
             [[ "${_err}" -ne  "$?" ]] && [[ "${_err}" -eq "${CODES[OK]}" ]] &&  _err=${CODES[ERROR]};
        done


    return ${_err};
}


#restore backups
#return CODES[ERROR], or CODES[OK] or CODES[NO_BACKUP_FILES]
restore() {


    local _err=${CODES[OK]};
    local _backups=($( ls -td   ${settings[DESTINATION]}/* ));

    [[ -z "${_backups}" ]] && return ${CODES[NO_BACKUP_FILES]};
    [[ -z "${_backups[0]}" ]] && return ${CODES[NO_BACKUP_FILES]};

    # wait until the LOCKFILE_READ is not removed: dumping are still in the progress
    for (( ;; )); do  [[ ! -f "${settings[SOURCE]}/${settings[LOCKFILE_READ]}" ]] && break; done;

    #lock the dumped directory: we forbid the bk-tree to dump to the directory
    touch  ${settings[SOURCE]}/${settings[LOCKFILE_WRITE]};
    ls ${_backups[0]}/*  |xargs -I {} cp {}  ${settings[SOURCE]}
    [[ "${_err}" -ne  "$?" ]] && [[ "${_err}" -eq "${CODES[OK]}" ]] &&  _err=${CODES[ERROR]};


    return ${_err};
}

#clean backup: after cleaning only the latest ${settings[STORE_BACKUP]} are kept
#return CODES[ERROR], or CODES[OK] or  CODES[NO_BACKUP_FILES]
clean_backup(){

    local _err=${CODES[OK]};
    local _num_backups=`ls -td ${settings[DESTINATION]}/* | wc -l`;
    local _tail_backups=;
    _tail_backups=$((_num_backups-settings[STORE_BACKUP])) && [[ $_tail_backups -lt 0 ]] && _tail_backups=0

    local _backup_to_clean=`ls -td ${settings[DESTINATION]}/* | tail -$_tail_backups`;

    [[ -z "${_backup_to_clean}" ]] && return ${CODES[NO_BACKUP_FILES]};

    for _dir in $_backup_to_clean
        do
            rm -r $_dir;
            [[ "${_err}" -ne  "$?" ]] && [[ "${_err}" -eq "${CODES[OK]}" ]] &&  _err=${CODES[ERROR]};
        done

    return ${_err};
}

#clean latest backup: removes the latest backup
#return CODES[ERROR], or CODES[OK] or CODES[NO_BACKUP_FILES]
clean_latest_backup(){

    local _err=${CODES[OK]};
    local _backup_to_clean=($( ls -td   ${settings[DESTINATION]}/* ));

    [[ -z "${_backup_to_clean[0]}" ]] && return  ${CODES[NO_BACKUP_FILES]};
    rm -r ${_backup_to_clean[0]};
    [[ "${_err}" -ne  "$?" ]] && [[ "${_err}" -eq "${CODES[OK]}" ]] &&  _err=${CODES[ERROR]};

    return ${_err};
}

# print the settings list
print_settings() {
    local all_settings="";

    for i in "${!settings[@]}"
        do
            all_settings=`echo "${all_settings}" "$i: ${settings[$i]}

 "`
        done

    echo
    echo
    echo "The list of settings:"
    echo
    echo
    echo "${all_settings}"

}

# print the backuplist
print_backup_list() {
    local all_settings="";

    for i in "${!backupList[@]}"
        do
            all_settings=`echo "${all_settings}" "$i: ${backupList[$i]}

 "`
        done

    echo
    echo
    echo "The list of backupList:"
    echo
    echo
    echo "${all_settings}"

}


# define a trap function to be called to handle SIGIN, SIGQUIT, SIGTERM and SIGHUP signals
# this trap function is important to handle deadlocks in bktree service!
trap_function(){

        if [  -f "${settings[SOURCE]}/${settings[LOCKFILE_WRITE]}" ]
            then
                echo "Backup system is stopping now, wait...";
                _bashlog CRIT 'Backup system is stopping now, wait...';
                echo  "BACKUP_STOP"  | nc  -q1   localhost 9770 2> /dev/null;
                echo  ${CODES[ERROR]}   | nc  -q1   localhost 9771 2> /dev/null;
                rm ${settings[SOURCE]}/${settings[LOCKFILE_WRITE]};
                exit ${CODES[ERROR]};
        else
                echo "Backup system is stopping now, wait...";
                _bashlog CRIT 'Backup system is stopping now, wait...';
                echo  "BACKUP_STOP"  | nc  -q1   localhost 9770 2> /dev/null;
                echo  ${CODES[ERROR]}   | nc  -q1   localhost 9771 2> /dev/null;                
                exit ${CODES[ERROR]};            
        fi
}

trap "trap_function" SIGTERM SIGINT SIGQUIT SIGHUP SIGKILL

#main function
main(){


    # set up parameters of the system
    backup_settings;

    # print all settings
    print_settings;

    # initialize  the system
    initialization;

    local _last_operation='';
    local _last_operation_timestamp='';



    # infinite loop
    # netcat is used to communicate with this back-up rotation system: the system read 9770 port and write response to the port 9771
    # possible keywords, with which the system operates,:

      # BACKUP_STOP --> to kill backupper
      # BACKUP  --> to backup data
      # RESTORE --> to restore data
      # SAVE_CRASHED --> happens when the main system(bktree) crashed during the saving data
      # LOAD_CRASHED --> happens when the main system(bktree) crashed during the loading data

    # Example of use: echo -ne 'BACKUP_STOP' | nc -q1  localhost 9770

    # an error code to be returned
    local _err=${CODES[OK]};
    while [ true ]
    do
          _bashlog INFO 'Backup system is ready';

        # release the dump directory for writting if it was locked from previous request
        [[   -f "${settings[SOURCE]}/${settings[LOCKFILE_WRITE]}" ]]   && rm ${settings[SOURCE]}/${settings[LOCKFILE_WRITE]};

	local request=`nc -l  -p 9770 2>/dev/null`

	#echo "debug: request:: " $request " :debug" ;
	#_bashlog DEBUG "debug: request::  $request  :debug" ;
        

        # here; stop of the service
        if [ "$request" == "BACKUP_STOP" ]
            then
                _err=${CODES[OK]};
                echo  $_err   | nc -q1  localhost 9771 2> /dev/null;
                _bashlog INFO 'main() has been killed....';                
                return ${CODES[OK]};
        # here: to restore  the data
        elif [ "$request" == "RESTORE" ]
            then
                restore 2> /dev/null;
                _err=$?;
                echo  $_err | nc -q1  localhost 9771 2> /dev/null;
                _last_operation="$request";
                _last_operation_timestamp=`date "+%Y-%m-%d %H:%M:%S"`;
                _bashlog INFO '[RESTORE]...restored';
        # here: to backup the data
        elif [ "$request" == "BACKUP" ]
            then
                backup 2> /dev/null;
                _err=$?;
                echo  $_err | nc  -q1 localhost 9771 2> /dev/null;
                clean_backup 2> /dev/null; # remove old backups
                _last_operation="$request";
                _last_operation_timestamp=`date "+%Y-%m-%d %H:%M:%S"`;

        # here: to backup the data
        elif [ "$request" == "LOAD_CRASHED" ]
            then
                _bashlog CRIT '[LOAD_CRASHED] has started...';
                backup CRASHED 2> /dev/null;
                if [ "$_last_operation" ==  "LOAD_CRASHED" ]
                   then
                    clean_latest_backup 2> /dev/null;
                fi
                restore 2> /dev/null;
                _err=$?;
                echo  $_err | nc  -q1   localhost 9771 2> /dev/null;
                _last_operation="$request";
                _last_operation_timestamp=`date "+%Y-%m-%d %H:%M:%S"`;
                _bashlog INFO '[LOAD_CRASHED]...restored';
# here: to backup the data
        elif [[ $request =~ BACKUP_MONITOR_[0-9.]+[^A-Za-z][0-9.]+ ]]
            then
                # forbid all external connection to the backup system during this request
                touch  ${settings[SOURCE]}/${settings[LOCKFILE_WRITE]};
                local _timeout=`echo $request | awk -F'_' '{print $3}'`;                
                ${settings[BACKUP_MONITOR_SERVICE]} --wait $_timeout --nameComponent=BACKUP_MONITOR --nameValue=STATUS --value=OK &
                if [ -z $_last_operation ]
                    then
                        ${settings[BACKUP_MONITOR_SERVICE]} --wait $_timeout --nameComponent=BACKUP_MONITOR --nameValue=LAST_OPERATION --value=BACKUP_INITIALIZED &
                        local __last_operation_timestamp=`date "+%Y-%m-%d %H:%M:%S"`;
                        ${settings[BACKUP_MONITOR_SERVICE]} --wait $_timeout --nameComponent=BACKUP_MONITOR --nameValue=LAST_OPERATION_TIMESTAMP --value="$__last_operation_timestamp" &
                    else
                        ${settings[BACKUP_MONITOR_SERVICE]} --wait $_timeout --nameComponent=BACKUP_MONITOR --nameValue=LAST_OPERATION --value=$_last_operation &
                        ${settings[BACKUP_MONITOR_SERVICE]} --wait $_timeout --nameComponent=BACKUP_MONITOR --nameValue=LAST_OPERATION_TIMESTAMP --value="$_last_operation_timestamp" &
                fi
                ${settings[BACKUP_MONITOR_SERVICE]} --wait $_timeout --nameComponent=BACKUP_MONITOR --nameValue=LAST_OPERATION_STATUS --value=$_err &
                
            
                echo  $_err   | nc -q1  localhost 9771 2> /dev/null;
            
        
        else # not supported operation
            # forbid all external connection to the backup system during this request
            touch  ${settings[SOURCE]}/${settings[LOCKFILE_WRITE]};
            _err=${CODES[ERROR]};
            _last_operation="$request";
            _last_operation_timestamp=`date "+%Y-%m-%d %H:%M:%S"`;
            echo  ${CODES[ERROR]} | nc   -q1   localhost 9771 2> /dev/null;

        fi

    done


_bashlog INFO 'main() has stoped....';
return ${CODES[OK]};
}


main;
trap - SIGINT SIGQUIT SIGTSTP SIGTERM SIGHUP SIGKILL
