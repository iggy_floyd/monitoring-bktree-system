#!/usr/bin/env bash

##################
#
#  Daemon control script
#
##################

### Logging service
## Logs dir: use a relative path


logsdir=$(pwd)/../logs

LOG_TAG=DAEMON_MONITOR
#. $logsdir/config.sh
. $logsdir/bashlog


# main parameters of the script
DAEMON=$MONITORRESPWAN
DAEMON_PATTERN1="cmxhttp.py"
DAEMON_PATTERN2="\"\\-startmonitor\""
KILLWORD="monitorkill"




# functions to start/stop/status a daemon
. /lib/lsb/init-functions



do_start () {


	log_daemon_msg "Starting $DAEMON_PATTERN1"
	_bashlog INFO "Starting $DAEMON_PATTERN1"

	if [ -z "$DAEMON" ] 
	then 
		 log_end_msg 1; _bashlog CRIT "\$DAEMON variable is not defiened"  ;exit 1;
	fi

	echo "ps aux | grep $DAEMON_PATTERN1 | grep $DAEMON_PATTERN2 | grep -v grep" | bash >/dev/null
	if [ $? -eq 0 ]
	then
		log_end_msg 1;
		_bashlog ALERT "The daemon is running" 
		return 1;
	else 
		 echo $DAEMON | bash;
		 local _status=$?
		 [  $_status -ne 0 ] &&  _bashlog CRIT "$DAEMON --> was failed" 
		 log_end_msg $_status
	fi

}



do_stop () {
	log_daemon_msg "Stopping $DAEMON_PATTERN1"
	 _bashlog INFO "Stopping $DAEMON_PATTERN1"

	[ -n "$DAEMON" ] &&  
	[  `which nc` ] && 
	echo "ps aux | grep $DAEMON_PATTERN1 | grep $DAEMON_PATTERN2 | grep -v grep" | bash >/dev/null &&
	echo  $KILLWORD | nc -q1  localhost 9760 &&
	( local _status=$?; [  $_status -ne 0 ] &&  _bashlog CRIT "$DAEMON --> was failed"; log_end_msg $_status)  || 
	(  _bashlog CRIT "\$DAEMON variable is not defiened"  ;log_end_msg 1 )




}


case "$1" in
 
start|stop)
	do_${1}
	;;
 
restart|reload|force-reload)
	do_stop
	do_start
	;;
 
status)
	log_daemon_msg  "Status of $DAEMON_PATTERN1"
	 _bashlog INFO "Status $DAEMON_PATTERN1"

	[ -n "$DAEMON" ]  && echo "ps aux | grep $DAEMON_PATTERN1 | grep $DAEMON_PATTERN2 | grep -v grep" | bash >/dev/null &&
         ( _status=$?; [  $_status -ne 0 ] &&  _bashlog CRIT "$DAEMON --> was failed"; log_end_msg $_status) ||
        (  _bashlog CRIT "\$DAEMON variable is not defiened"  ;log_end_msg 1 )

;;
*)
echo "Usage: $0 {start|stop|restart|status}"
exit 1
;;
 
esac
exit 0
