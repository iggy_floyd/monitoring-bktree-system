#!/usr/bin/env bash

# prints a help message out
message() {


 

    cat << _END > _tmp_file_$$

		Hi!

	You can try the following options:
		* $1 start  -- to start all daemons
		* $1 stop   -- to stop all daemons
		* $1 restart -- to restart all daemons
		
_END


       cat _tmp_file_$$
       rm -f _tmp_file_$$;


}


do_stop() {
   

    local a=`ps aux | grep "cmxhttp.py -startmonitor" | grep -v grep | wc -l`; while [[ $a -gt 0 ]]; do ./daemon_monitor.sh  stop > /dev/null; a=`ps aux | grep "cmxhttp.py -startmonitor" | grep -v grep | wc -l`; done
    local b=`ps aux | grep "monitor_resources.sh"  | grep "\-startcayleymonitor" | grep -v grep | wc -l`; while [[ $b -gt 0 ]]; do ./daemon_monitor_cayley.sh  stop > /dev/null; b=`ps aux | grep "monitor_resources.sh"  | grep "\-startcayleymonitor" | grep -v grep | wc -l`; done
    local c=`ps aux | grep "backup_rotation_bktree_dumps.sh -startbackup" | grep -v grep | wc -l`; while [[ $c -gt 0 ]]; do echo "BACKUP_STOP" | nc -q1 localhost 9770; ./daemon_backup_rotation.sh  stop > /dev/null; c=`ps aux | grep "backup_rotation_bktree_dumps.sh -startbackup" | grep -v grep | wc -l`; done

}


do_start(){
	./daemon_backup_rotation.sh  start;
        ./daemon_monitor.sh  start;
	./daemon_monitor_cayley.sh start
}

do_status(){
	./daemon_backup_rotation.sh  status;
        ./daemon_monitor.sh  status;
	./daemon_monitor_cayley.sh status;
}

# the main enrty point
main() {

case "$2" in
 
	start|stop|status)
		do_${2}
	;;

	restart|reload|force-reload)
		do_stop
		do_start
	;;
 
	*)
	echo;
	echo "Usage: $1 {start|stop|restart}";
	echo;
	message;	
	exit 1;
	;;
 
esac
exit 0

}	



main $0 $1;

