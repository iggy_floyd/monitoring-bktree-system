
# declare settings array
declare -A settings # please note, that associative array can be created in the bash with version >4.0

_setuppath=`pwd`




message() {


 

    cat << _END > _tmp_file_$$

		Hi User!

        This is a setup script of the BK-tree package.
        The script properly defines  all environment variables 
        required by the BK-tree.


_END


       cat _tmp_file_$$
       rm -f _tmp_file_$$;

    

}


#
# please, note that relative  paths will be used
#


cayley_settings() {

# cayley path
        settings[CAYLEYPATH]="${_setuppath}/../cayley/cayley_0.4.0_linux_amd64/";

# cayley configpath
        settings[CAYLEYCONFIGPATH]=${_setuppath}/../cayley/configuration/


# gremlin api path
        settings[GREMLINAPIPATH]=${_setuppath}/../../../application/modules/FPSCore/Class/Webservice/Client/Cayley/JS

# gremlin api file
        settings[GREMLINAPI]="gremlin_api.txt"

# cayley Backend Database
            settings[CAYLEYBACKEND]="leveldb"

# cayley Backend Database path
            settings[CAYLEYBACKENDPATH]=$HOME/fpsdb;


# cayley Backend Database initialization
            settings[CAYLEYBACKENDINI]='yes'

# cayley Backend Database configuraton
            settings[CAYLEYBACKENCONF]=${settings[CAYLEYCONFIGPATH]}cayley_${settings[CAYLEYBACKEND]}.cfg


# respawn service
            settings[RESPAWN]=${_setuppath}/respawn_double_fork.sh


}


bktree_settings() {


# bk-tree ini file
            settings[BKTREEINI]=${_setuppath}/config.ini


# prefix of the created files with trees
            settings[PREFIXDB]="cayley_fps_"


# port number 
            settings[BKTREEPORT]=8888

#do-timing
            settings[BKTREEDOTIMING]=0



# path to bktree executable
 
            settings[BKTREEPATH]=${_setuppath}/../bin/bktree

# here: bk-tree cmd line
            local dotiming;            
            [[ "${settings[BKTREEDOTIMING]}" -eq 1 ]] && dotiming="--do-timing";


            settings[BKTREECMD]="${settings[BKTREEPATH]} --prefixDB ${DUMP_DIR_BKTREE}${settings[PREFIXDB]} -p ${settings[BKTREEPORT]} $dotiming --do-threads --entries2dump --entries2dump-size 10000 --log_file $LOG_FILE_BKTREE --log_prio_filter $LOG_PRIO_FILTER"

}


backup_rotation_dump_settings() {

# path to the  backup system
    settings[BACKUPPATH]=${_setuppath}/backup_rotation_bktree_dumps.sh;

# cmd of backup system
    settings[BACKUPCMD]="${settings[BACKUPPATH]} -startbackup";
}



monitor_settings() {

# path to the  backup system
    settings[MONITORPATH]=${_setuppath}/../cmx-sources-V2_1_5/cmw-cmx-python/target/resources/cmxhttp.py;

# cmd of backup system
    settings[MONITORCMD]="${settings[MONITORPATH]} -startmonitor";
}


monitor_cayley_settings() {

# path to the  backup system
    settings[MONITORRESOURCEPATH]=${_setuppath}/monitor_resources.sh;

# cmd of backup system
    settings[MONITORCAYLEYCMD]="${settings[MONITORRESOURCEPATH]} ${settings[CAYLEYPATH]}/cayley CAYLEY_RESOURCES -startcayleymonitor";

## for testing purpose!
## please, remove it in the release
    settings[MONITORCAYLEYCMD]="${settings[MONITORRESOURCEPATH]} backup_rotation_bktree_dumps.sh CAYLEY_RESOURCES  -startcayleymonitor";
}




print_settings() {


local all_settings="";

    for i in "${!settings[@]}"
        do
            all_settings=`echo "${all_settings}" "$i: ${settings[$i]} 

 "`
        done

    echo "The list of exported variables:"
    echo 
    echo
    echo "${all_settings}" 

} 




configuring() {


        cayley_settings; # 3) set cayley parameters
        bktree_settings; # 4) set bktree parameters
        backup_rotation_dump_settings; # set backup_rotation system parameters
        monitor_settings; 
        monitor_cayley_settings;
        print_settings;   # 5) to print out all settings

    

}




exporting() {


    export CAYLEYPATH=${settings[CAYLEYPATH]};
    export CAYLEYCONFIGPATH=${settings[CAYLEYCONFIGPATH]};
    export GREMLINAPIPATH=${settings[GREMLINAPIPATH]};
    export GREMLINAPI=${settings[GREMLINAPI]};

    export CAYLEYBACKEND=${settings[CAYLEYBACKEND]};


    # if cayley requires initialization
    [[ "${settings[CAYLEYBACKENDINI]}" == "yes" ]]  && [ `which $CAYLEYPATH/cayley` ] && $CAYLEYPATH/cayley init --db="${settings[CAYLEYBACKEND]}" --dbpath="${settings[CAYLEYBACKENDPATH]}";

    local _conf=`eval  echo  ${settings[CAYLEYBACKENCONF]}`
    export CAYLEYCMD="$CAYLEYPATH/cayley http --db=${settings[CAYLEYBACKEND]} -log_dir=$LOG_DIR_CAYLEY  --dbpath=${settings[CAYLEYBACKENDPATH]}  --assets $CAYLEYPATH -config=${settings[CAYLEYBACKENCONF]} --v=${LOG_PRIO_FILTER_CAYLEY}";
    
    export BKTREEINI=${settings[BKTREEINI]};
    export BKTREECMD=${settings[BKTREECMD]};
    export BACKUPCMD=${settings[BACKUPCMD]};
    export MONITORCMD=${settings[MONITORCMD]};
    echo ${settings[MONITORCAYLEYCMD]}

    # you can test MONITORCAYLEYCMD as 
    ##echo $MONITORCAYLEYCMD | sed 's/\\"/"/g' | sh
    export MONITORCAYLEYCMD="${settings[MONITORCAYLEYCMD]}";
    export RESPAWNSCRIPT=${settings[RESPAWN]};

    if [ -n $RESPAWNSCRIPT ] && [ ! "$RESPAWNSCRIPT" == "" ]  
	then
		 export CAYLEYRESPWAN="( $RESPAWNSCRIPT \"$CAYLEYCMD\" 1 \"cayley http\" \"cayleykill\" & ) &"
	fi

    if [ -n $RESPAWNSCRIPT ] && [ ! "$RESPAWNSCRIPT" == "" ]  
	then
		 export BKTREERESPWAN="( $RESPAWNSCRIPT \"$BKTREECMD\" 1 \"\\--prefixDB ${DUMP_DIR_BKTREE}${settings[PREFIXDB]} -p ${settings[BKTREEPORT]}\" \"bktreekill\" & ) &"
	fi


    if [ -n $RESPAWNSCRIPT ] && [ ! "$RESPAWNSCRIPT" == "" ]
	then
		 export BACKUPROTATIONRESPWAN="( $RESPAWNSCRIPT \"$BACKUPCMD\" 1 \"\\-startbackup\" \"backuprotationkill\" & ) &"
	fi

    if [ -n $RESPAWNSCRIPT ] && [ ! "$RESPAWNSCRIPT" == "" ]
	then
		 export MONITORRESPWAN="( $RESPAWNSCRIPT \"$MONITORCMD\" 1 \"\\-startmonitor\" \"monitorkill\" & ) &"
	fi

    if [ -n $RESPAWNSCRIPT ] && [ ! "$RESPAWNSCRIPT" == "" ]
	then
		 export MONITORCAYLEYRESPAWN="( $RESPAWNSCRIPT \"$MONITORCAYLEYCMD\" 1 \"\\-startcayleymonitor\" \"cayleymonitorkill\" & ) &"
	fi


}



unsetting() {

    unset CAYLEYPATH CAYLEYCONFIGPATH GREMLINAPIPATH GREMLINAPI CAYLEYCMD BKTREEINI BKTREECMD BACKUPCMD MONITORCMD MONITORCAYLEYCMD

}



# reading ini file for further initialization
source <(grep = config.ini |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\2/'" | sh)


# Dump directory initialization
export DUMP_DIR_BKTREE="$cayley_bktree_dump_dirname"

[[ -n $DUMP_DIR_BKTREE ]] && mkdir -p ${DUMP_DIR_BKTREE} 2> /dev/null

# Logging service initialization
LOG_TAG=SETUP
export LOG_FILE="$cayley_bktree_logging_path"
export SYSLOG_FACILITY_LOGGER=local0
export LOG_FILE_BKTREE="$LOG_FILE"
export LOG_DIR_CAYLEY=`dirname $LOG_FILE`
export LOG_PRIO_FILTER="$cayley_bktree_logging_priorityFilter"

#see https://google-glog.googlecode.com/svn/trunk/doc/glog.html about cayley log levels:
# The numbers of severity levels INFO, WARNING, ERROR, and FATAL are 0, 1, 2, and 3, respectively.
if [ -n $LOG_PRIO_FILTER ]
    then
	     # cayley log level filter
         export LOG_PRIO_FILTER_CAYLEY=0
         [[ $LOG_PRIO_FILTER -eq 0 ]] &&  export LOG_PRIO_FILTER_CAYLEY=3
         [[ $LOG_PRIO_FILTER -eq 1 ]] &&  export LOG_PRIO_FILTER_CAYLEY=3
         [[ $LOG_PRIO_FILTER -eq 2 ]] &&  export LOG_PRIO_FILTER_CAYLEY=2
         [[ $LOG_PRIO_FILTER -eq 3 ]] &&  export LOG_PRIO_FILTER_CAYLEY=1

         # a bashlog level filter
         [[ $LOG_PRIO_FILTER -eq 0 ]] &&  export LOG_LEVEL=EMERGENCY
         [[ $LOG_PRIO_FILTER -eq 1 ]] &&  export LOG_LEVEL=ALERT
         [[ $LOG_PRIO_FILTER -eq 2 ]] &&  export LOG_LEVEL=CRIT
         [[ $LOG_PRIO_FILTER -eq 3 ]] &&  export LOG_LEVEL=ERR
         [[ $LOG_PRIO_FILTER -eq 4 ]] &&  export LOG_LEVEL=WARN
         [[ $LOG_PRIO_FILTER -eq 5 ]] &&  export LOG_LEVEL=NOTICE
         [[ $LOG_PRIO_FILTER -eq 6 ]] &&  export LOG_LEVEL=INFO
         [[ $LOG_PRIO_FILTER -eq 6 ]] &&  export LOG_LEVEL=DEBUG
        

    else
        export LOG_PRIO_FILTER_CAYLEY=0
fi


if  [ -z  $BL_LEVEL  ]  
then

	logsdir=$(pwd)/../logs
	cd $logsdir
#	. config.sh
	. bashlog
	cd - 2>&1 /dev/null
fi



# main program goes here
_bashlog INFO 'Initialization starts...'

message; # 2) greetings

unsetting; # 2.2) clearing all environment variables previously set

configuring

exporting; # 7) exporting the environment variables

_bashlog INFO 'Initialization has finished'

