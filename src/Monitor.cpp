#include "Monitor.hpp"


 cmw::cmx::ComponentPtr Monitor::component;
 

 
 void Monitor::initialize(const char * nameComponent) 
 {
    cmw::cmx::ProcessComponent::update();
    Monitor::component = cmw::cmx::Component::create(nameComponent);
         
 }

 
/**

how-to compile:
@compile@ g++ -c Monitor.cpp -o Monitor.o -Wno-long-long -Wall -pedantic  -g -O2 -I. -I../include  -rdynamic -fnon-call-exceptions  -std=c++11   -I../cmx-sources-V2_1_5/cmw-cmx-c/target/include -I../cmx-sources-V2_1_5/cmw-cmx-cpp/target/include

how-to link: 
@link@	  ar cru libMonitor.a *.o
@link@    ranlib libMonitor.a
@link@    mv libMonitor.a ../lib/libMonitor.a
@link@    gcc -shared -o ../lib/libMonitor.so  `( cd ..; ./config.sh --libs )`  

**/ 
