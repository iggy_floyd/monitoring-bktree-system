/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
extern "C"
{
#include <stdio.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <float.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <getopt.h>
#include <fnmatch.h>

#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/shm-private.h>
#include <cmw-cmx/registry.h>
#include <cmw-cmx/log.h>
}
#include <iostream>
#include <cmw-cmx-cpp/Log.h>
using namespace cmw::cmx;

void help()
{
    std::cout << "-h             help" << std::endl;
    std::cout << "-n <name>      filter by compnent name" << std::endl;
    std::cout << "-p <proces_id> filter by proces id" << std::endl;
}

/**
 * This program dumps all cmx datastructures regardless of any concurrent access.
 */
int main(int argc, char *argv[])
{
    cmx_component_info * component_info;
    int len;

    int opt;
    bool filter_comp_name = false;
    std::string filter_comp_name_value;
    bool filter_process_id = false;
    int filter_process_id_value;

    while ((opt = getopt(argc, argv, "n:p:h")) != -1)
    {
        switch (opt)
        {
            case 'n':

                filter_comp_name = true;
                filter_comp_name_value = optarg;
                break;
            case 'p':
                filter_process_id = true;
                filter_process_id_value = atoi(optarg);
                break;
            case 'h':
            default:
                help();
                exit(0);
                break;
        }
    }

    assert(E_CMX_SUCCESS == cmx_registry_discover(&component_info, &len));

    for (int i = 0; i < len; i++)
    {
        cmx_shm * cmx_shm_ptr;
        const cmx_component_info * cmx_component = &component_info[i];
        if (filter_process_id)
        {
            if (cmx_component->process_id != filter_process_id_value)
            {
                continue;
            }
        }
        if (filter_comp_name)
        {
            if (fnmatch(filter_comp_name_value.c_str(), cmx_component->name, 0) != 0)
            {
                continue;
            }
        }

        if (E_CMX_SUCCESS != cmx_shm_open_ro(cmx_component->process_id, cmx_component->name, &cmx_shm_ptr))
        {
            Log::audit(LOG_POSITION(), "skip %d %s", cmx_component->process_id, cmx_component->name);
            continue;
        }
        printf("microseconds=%"PRIu64"\n", cmx_common_current_time_usec());
        printf("struct cmx_shm_t {\n");
        printf("  CMX_TAG=\"%*s\"\n", CMX_SHM_TAG_LEN, cmx_shm_ptr->CMX_TAG);
        printf("  process_id=%d\n", cmx_shm_ptr->process_id);
        printf("  name=\"%*s\"\n", CMX_NAME_SZ, cmx_shm_ptr->name);
        printf("  value_state[]  (omitted)\n");
        printf("  value=\n");
        for (int j = 0; j < CMX_NUMBER_OF_VALUES; j++)
        {
            if (cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_FREE)
            {
                // ignore
            }
            else if (cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_MARKED_GC)
            {
                printf("  j = %d state = CMX_SLOT_STATE_MARKED_GC\n", j);
            }
            else if (cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_OCCUPIED)
            {
                printf("  j = %d state = CMX_SLOT_STATE_OCCUPIED\n", j);
            }
            else if (cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_PAYLOAD)
            {
                printf("  j = %d state = CMX_SLOT_STATE_PAYLOAD\n", j);
            }

            if (cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_SET
                    || cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_UPDATE)
            {
                printf("  struct cmx_shm_slot_value { state = %s\n",
                        (cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_SET) ? "CMX_SLOT_STATE_SET" :
                                "CMX_SLOT_STATE_UPDATE");
                printf("    id=%hu\n", cmx_shm_ptr->value[j].value.id);
                printf("    type=%d\n", cmx_shm_ptr->value[j].value.type);
                printf("    mtime=%" PRIu64 "\n", cmx_shm_ptr->value[j].value.mtime);
                printf("    name=\"%*s\"\n", CMX_NAME_SZ, cmx_shm_ptr->value[j].value.name);
                printf("    txn=%"PRIu64"\n", cmx_shm_ptr->value[j].value.txn);
                switch (cmx_shm_ptr->value[j].value.type)
                {
                    case CMX_TYPE_INT64:
                        printf("    value._int64=%" PRIu64 "\n", cmx_shm_ptr->value[j].value.value._int64);
                        break;
                    case CMX_TYPE_FLOAT64:
                        printf("    value._float64=%lf\n", cmx_shm_ptr->value[j].value.value._float64);
                        break;
                    case CMX_TYPE_BOOL:
                        printf("    value._bool=%s\n", (cmx_shm_ptr->value[j].value.value._bool) ? "true" : "false");
                        break;
                    case CMX_TYPE_STRING:
                        printf("    value._string={size=%hu,current_size=%hu,next_index=%hu,next_id=%hu}\n",
                                cmx_shm_ptr->value[j].value.value._string.size,
                                cmx_shm_ptr->value[j].value.value._string.current_size,
                                cmx_shm_ptr->value[j].value.value._string.next_index,
                                cmx_shm_ptr->value[j].value.value._string.next_id);
                }
                printf("  }\n");
            }
            else if (cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_PAYLOAD)
            {
                cmx_shm_slot_payload_t * payload_slot = &cmx_shm_ptr->value[j].value_payload;
                printf("  struct cmx_shm_slot_payload { id=%04d, next_id=%04d, next_index=%04d }\n", payload_slot->id,
                        payload_slot->next_id, payload_slot->next_index);
            }
        }
        printf("}\n");
        printf("microseconds=%"PRIu64"\n", cmx_common_current_time_usec());
    }

    free(component_info);
}
