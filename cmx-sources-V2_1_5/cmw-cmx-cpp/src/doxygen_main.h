/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/*! \mainpage CMX C++ Library
 * \section ABOUT_CMX About CMX C++
 * \htmlonly
 * <h2>This is CMX-cpp version: $(VERSION)</h2>
 * \endhtmlonly
 *
 * For more information visit CMX on https://cern.ch/cmx and/or https://wikis.cern.ch/display/MW/CMX
 *
 * \section CHANGELOG Changelog
 * The changelog of CMX is a shared document for all CMX products.
 * For the combined Changelog refer to the CMX C API documentation.
 *
 * The C++ API is a object-oriented wrapper around the C API. Please consult the Doxygen API Documentation
 * for documentation of function calls.
 *
 * \section HOWTO Howto use CMX for C++
 * \subsection HOWTO_REGISTER_PROCESS Register your process with CMX
 * After you configured compiler/linker settings for the CMX library you can go into your code and
 * start making use of CMX.
 *
 * If you are developing an executable/application (not a supporting library),
 * the first step is to register (and update regularly) basic process information.
 *
 * \code
 * #include <cmw-cmx-cpp/ProcessComponent.h>
 * ...
 * using namespace cmw::cmx;
 * ...
 * ProcessComponent::update();
 * \endcode
 *
 * The first the this call creates a record in the CMX main-registry about your process. The next times it will
 * update all non-static information such as cpu usage.
 *
 * You should consider calling `Process::update()` from time to time to update those metrics.
 *
 * \subsection HOWTO_REGISTER_METRIC Register and update the Metrics
 *
 * Before you can update metrics you need to assign meaningful names (see xref:naming[Naming conventions])
 * and register them in CMX.
 *
 * There a two different abstraction levels to expose metrics which will be described with examples.
 *
 * \b Using CMX in a direct way
 *
 * By creating Components "by hand" and add metrics you are in full control of your metrics.
 *
 * \code
 * #include <cmw-cmx-cpp/Component.h>
 * ...
 * using namespace cmw::cmx;
 * ...
 * ComponentPtr c = Component::create("TestComponent");
 * CmxInt64 mTestInt64 = c->newInt64("TestInt64");
 * mTestInt64 = 123;
 * CmxString mTestString = c->newString("TestString",116);
 * mTestString = "TestString ... upto 116 chars...";
 * \endcode
 *
 * If you pass a second parameter `ignoreError` to `Component::create` no exception will be thrown in case the operation
 * fails, instead you will receive a dummy Component. All operations will silently fail.
 *
 * \b Using CMX with the CMXSupport class
 *
 * The C++ Class `CMXSupport` makes it even easier to extend a class with support for CMX Metrics.
 *
 * \b Class Header
 *
 * \code
 * #include <cmw-cmx-cpp/CmxSupport.h>
 * ...
 * using namespace cmw::cmx;
 * ...
 * class AbstractEventSource : public Thread, private cmw::cmx::CmxSupport
 * {
 * ...
 *     /// CMX Variables
 *     cmw::cmx::CmxInt64 cmxFiredEventCount_;
 * };
 * \endcode
 *
 * 'Class Body'
 * \code
 * ...
 * AbstractEventSource::AbstractEventSource(const std::string& eventSourceName,
 *                                          EventSourceType type,
 *                                          const std::string& className,
 *                                          const boost::shared_ptr<Diagnostics>& diagnostics) :
 *     ...
 *     cmxFiredEventCount_(newInt64(("AbstractEventSource." + eventSourceName), "firedEventCount"))
 * {
 *     ...
 *     message << "Creating event source '" << name_ << "'";
 *     ...
 * }
 *
 * void AbstractEventSource::run()
 * {
 *     ..
 *     cmxFiredEventCount_ = cmxFiredEventCount_  + 1;
 *     ..
 * }
 * \endcode
 */

/**
 * \b fast-writer-cpp: Like the C-Version this programm tries to update a cmx value as fast as possible.
 * \example demo/fast-writer-cpp.cpp
 */

/**
 * \b Demo1 Example of CMX integration using the CMXSupport class.
 *
 * The Header of the Cmx support enabled class
 * \include cmw-cmx-cpp/demo/Demo1.h
 *
 * The Implementation of Demo1
 * \include cmw-cmx-cpp/demo/Demo1.cpp
 *
 * The main()
 * \example demo/demo1.cpp
 */

/**
 * \b Demo2: Like Demo1 but with hidden implementation using the CMXSupport class.
 *
 * The Header of a class holding a pointer to the CMXSupport enabled class
 * \include cmw-cmx-cpp/demo/Demo2.h
 *
 * The Implementation of Demo2
 * \include cmw-cmx-cpp/demo/Demo2.cpp
 *
 * The main()
 * \example demo/demo2.cpp
 */

/**
 * \b example1: Like example1 in C but using C++.
 *
 * Shows how to expose metrics in the 'direct way' like it is described in the user-guide.
 * \example demo/example1.cpp
 */
