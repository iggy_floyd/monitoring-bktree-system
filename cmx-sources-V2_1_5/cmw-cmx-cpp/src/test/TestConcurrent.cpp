/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file \u201cCOPYING\u201d.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>
#include <algorithm>
#include <sstream>
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/Log.h>
#include <cmw-cmx-cpp/CmxTypes.h>
#include <cmw-cmx-cpp/Registry.h>
#include <pthread.h>

extern "C"
{
#include <cmw-cmx/common.h>
}
using namespace cmw::cmx;

static int run = 0;
static int read_count = 0;
static const int rounds = 80000;

static ComponentPtr component;

void * thread_string_read(void *arg)
{
    CmxString * s = (CmxString *) arg;
    while (!run)
        sched_yield();
    while (run)
    {
        try
        {
            const std::string value = s->operator std::string();
            if (not (*value.begin() == 'A' && *(value.begin() + value.size() - 1) == 'Z')
                    and not (*value.begin() == 'Z' && *(value.begin() + value.size() - 1) == 'A'))
            {
                std::cout << "Failed with value=" << value << " read_count=" << read_count << std::endl;
                read_count = 0;
                break;
            }
            read_count++;
        }
        catch (CmxConcurrentModificationException & e)
        {
            // pass
            sched_yield();
        }
    }
    return NULL;
}

void * thread_string_write(void *arg)
{
    char buf[123];
    CmxString * s = (CmxString *) arg;
    while (!run)
        sched_yield();
    for (int i = 0; i < 500000; i++)
    {
        buf[0] = (i % 2) ? 'A' : 'Z';
        buf[1] = 'X';
        buf[i % (123 - 4 - 1) + 2] = (char)('0' + (char) (i % 10));
        buf[(i % (123 - 4 - 1)) + 3] = 'X';
        buf[(i % (123 - 4 - 1)) + 4] = (i % 2) ? 'Z' : 'A';
        buf[(i % (123 - 4 - 1)) + 5] = '\0';

        s->operator =(std::string(buf));
    }
    return NULL;
}

TEST(TestConcurrent, test_string)
{
    if (getenv("LD_PRELOAD") != NULL)
    {
        if (std::string(getenv("LD_PRELOAD")).find("valgrind") != std::string::npos)
        {
            std::cout << "This test is disabled under valgrind" << std::endl;
            return;
        }
    }
    pthread_t t_read, t_write;
    component = Component::create("TestConcurrent");
    CmxString s = component->newString("testString", 123);
    s = "AZ";

    assert(0==pthread_create(&t_read,NULL, thread_string_read, &s));
    // TODO spawn 3 writers
    assert(0==pthread_create(&t_write, NULL, thread_string_write, &s));
    run = 1;
    pthread_join(t_write, NULL);
    run = 0;
    pthread_join(t_read, NULL);
    ASSERT_GT(read_count, rounds * 0.35); // expect success of at least 35%
}
