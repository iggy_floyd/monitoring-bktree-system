/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_IMMUTABLECOMPONENT
#define HAVE_CMW_CMX_IMMUTABLECOMPONENT

#include <functional>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <cmw-cmx-cpp/CmxTypes.h>

namespace cmw
{
namespace cmx
{
class ImmutableComponent;
typedef boost::shared_ptr<ImmutableComponent> ImmutableComponentPtr;

/**
 * \brief Predicate to filter over Value names
 *
 * for use with algorithms like std::find_if().
 */
class ValuePredicateName : std::unary_function<const CmxRef &, bool>
{
public:
    ValuePredicateName(const std::string name) :
                    name_(name)
    {
    }
    result_type operator()(argument_type ref) const
    {
        return ref.name() == name_;
    }
private:
    const std::string name_;
};

/**
 * \brief ComponentIterator. Iterates over all values in a component.
 */
class ComponentIterator : public std::iterator<std::input_iterator_tag, CmxRef>
{
public:
    ComponentIterator(ImmutableComponentPtr immutableComponent, int next) :
                    immutableComponent_(immutableComponent),
                    pos_(next),
                    value_type_(-1),
                    value_handle_(-1)
    {
        init();
    }

    // get value at current position
    CmxRef operator*();

    ComponentIterator operator++(); // ++operator

    /// \see operator++()
    ComponentIterator operator++(int); // operator++

    bool operator==(const ComponentIterator& __x) const
    {
        return (pos_ == __x.pos_);
    }

    bool operator!=(const ComponentIterator& __x) const
    {
        return (pos_ != __x.pos_);
    }

    static const int end = 0x0fffffff;
private:
    void init();
    ImmutableComponentPtr immutableComponent_;
    int pos_;
    int value_type_;
    int value_handle_;
};

/**
 * \brief CMX ImmutableComponent
 */
class ImmutableComponent
{
    friend class ComponentIterator;
public:
    /**
     * \brief the iterator type used by begin() and end().
     */
    typedef ComponentIterator iterator;

    CmxImmutableInt64::c_type getValue(const CmxImmutableInt64 & ref);

    CmxImmutableFloat64::c_type getValue(const CmxImmutableFloat64 & ref);

    CmxImmutableBool::c_type getValue(const CmxImmutableBool & ref);

    /// test
    CmxImmutableString::c_type getValue(const CmxImmutableString & ref);

    /// return the modification time of a value.
    uint64_t getMTime(const CmxRef & ref);

    /// return a string representation of the value.
    std::string getValueAsString(const CmxRef & ref);

    /// get name of a Value by passing the Cmx Reference base class.
    std::string getName(const CmxRef & ref);

    /// name of the component
    std::string name() const;

    /// process id of the process owning/created this component.
    int processId() const;

    /**
     * \brief get a iterator to the first value in this component.
     */
    iterator begin();

    /// get a iterator that points after the last value in this component.
    iterator end();

    /// Returns true if this component behave neutral instead of throwing exceptions.
    bool isIgnoreErrors();

    /// Returns true if this component is not backed by a real CMX component.
    bool isDummyMode();

    /**
     * \brief set if this component shall ignore errors
     *
     * Set whether this component ignores errors and - if case of read operations -
     * returns default values.
     * Otherwise these operation would throw exceptions.
     */
    void setIgnoreErrors(const bool ignoreErrors);

    /// get read retry count
    int getReadRetry();

    /*
     * \brief Read Retry count in case of concurrent modifications.
     *
     * Set the number of times a read is repeated in case of
     * a concurrent modification.
     *  - negative values are invalid
     *  - 0 means infinite retries
     *  - >0, e.g. 5 means: 5 attempts are made to read a value before failing with CmxConcurrentModificationException.
     *  Default is 20. This is sufficient in most scenarios even with high-update concurrent processes.
     *  You may want to lower this (in some cases to 1) if your reader process has real-time constraints.
     */
    void setReadRetry(const int readRetry);

    /**
     * \brief Unmaps the Component from the process Space.
     */
    ~ImmutableComponent();
protected:
    void * cmx_shm_ptr_;
    bool dummyMode_;
    bool ignoreErrors_;
    int readRetry_;
    boost::weak_ptr<ImmutableComponent> weakImmutableSelf;
    static ImmutableComponentPtr open(void * cmx_shm_ptr);
    /**
     * \param cmx_shm_ptr   If this parameter is null then the component will switch to 'dummy-mode'.
     */
    ImmutableComponent(void * cmx_shm_ptr);
private:
    ImmutableComponent(const Component& that); // = delete
};

} // namespace cmx
} // namespace cmw
#endif
