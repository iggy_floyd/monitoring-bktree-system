/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_PROCESSCOMPONENT
#define HAVE_CMW_CMX_PROCESSCOMPONENT

namespace cmw
{
namespace cmx
{

/**
 * \see cmw_cmx_process_update() in cmw-cmx-c
 */
class ProcessComponent
{
public:
    /**
     * \brief Update metrics.
     * Update the metrics in the process component. If this is the first call then
     * this will create the component.
     * This function is intended to be used in the code forming the final executable. Not
     * in libraries where the developer of the final appplication have no control.
     *
     * \throw This method never throws any exception. However it may generate log error messages.
     */
    static void update();
};

} // namespace cmx
} // namespace cmw

#endif
