/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_TYPES
#define HAVE_CMW_CMX_TYPES
#include <boost/shared_ptr.hpp>
#include <cmw-cmx-cpp/CmxException.h>
extern "C"
{
#include <stdint.h>
}
namespace cmw
{
namespace cmx
{

class Component;
typedef boost::shared_ptr<Component> ComponentPtr;
class ImmutableComponent;
typedef boost::shared_ptr<ImmutableComponent> ImmutableComponentPtr;

class CmxImmutableInt64;
class CmxInt64;
class CmxImmutableFloat64;
class CmxFloat64;
class CmxImmutableBool;
class CmxBool;
class CmxImmutableString;
class CmxString;

struct CmxTypeTagInt64
{
    static const int cmx_type;
};
struct CmxTypeTagFloat64
{
    static const int cmx_type;
};
struct CmxTypeTagBool
{
    static const int cmx_type;
};
struct CmxTypeTagString
{
    static const int cmx_type;
};

template<typename type_tag>
struct CmxTypeInfo;

template<>
struct CmxTypeInfo<CmxTypeTagInt64>
{
    typedef int64_t c_type;
    typedef CmxImmutableInt64 immutable_type;
    typedef CmxInt64 mutable_type;
    static const c_type neutral;
};

template<>
struct CmxTypeInfo<CmxTypeTagFloat64>
{
    typedef double c_type;
    typedef CmxImmutableFloat64 immutable_type;
    typedef CmxFloat64 mutable_type;
    static const c_type neutral;
};

template<>
struct CmxTypeInfo<CmxTypeTagBool>
{
    typedef bool c_type;
    typedef CmxImmutableBool immutable_type;
    typedef CmxBool mutable_type;
    static const c_type neutral;
};

template<>
struct CmxTypeInfo<CmxTypeTagString>
{
    typedef std::string c_type;
    typedef CmxImmutableString immutable_type;
    typedef CmxString mutable_type;
    static const c_type neutral;
};

/**
 * \brief Base CMX Value Reference Type
 */
struct CmxRef
{
    friend class ComponentIterator;
    template<typename _CmxTypeTag>
    friend typename CmxTypeInfo<_CmxTypeTag>::immutable_type cmx_cast(const CmxRef & ref);
    const int type_;
    // if not set this variable is considered as inactive.
    // set does nothing. get returns CMXTypeToCType::neutral
    ImmutableComponentPtr immutableComponent_;
    const int value_handle_;
protected:
    static ImmutableComponentPtr pointerConvert(ComponentPtr p);
    CmxRef(int type, ImmutableComponentPtr immutableComponent, int value_handle) :
                    type_(type),
                    immutableComponent_(immutableComponent),
                    value_handle_(value_handle)
    {
    }
    /// Creates a dead CmxRef. Returns neutral values and don't set anything.
    CmxRef(int type, ImmutableComponentPtr immutableComponent) :
                    type_(type),
                    immutableComponent_(immutableComponent),
                    value_handle_(0)
    {
    }
public:
    /// get the modification time
    uint64_t mtime() const;
    /// get the name
    std::string name() const;
    /// get the cmx type id.
    int type() const
    {
        return type_;
    }
    const std::string type_name() const;
};

// ==================== Immutable*

/**
 * \brief CMX Immutable Value Reference to INT64 Value
 */
struct CmxImmutableInt64 : public CmxRef
{
    CmxImmutableInt64(ImmutableComponentPtr immutableComponent, int value_handle) :
                    CmxRef(CmxTypeTagInt64::cmx_type, immutableComponent, value_handle)
    {
    }
    /// \see CmxRef::CmxRef(int)
    CmxImmutableInt64(ImmutableComponentPtr immutableComponent) :
                    CmxRef(CmxTypeTagInt64::cmx_type, immutableComponent)
    {
    }
    typedef CmxTypeInfo<CmxTypeTagInt64>::c_type c_type;

    // get value in native C++ Type
    operator c_type();
};

/**
 * \brief CMX Immutable Value Reference to FLOAT64 Value
 */
struct CmxImmutableFloat64 : public CmxRef
{
    CmxImmutableFloat64(ImmutableComponentPtr immutableComponent, int value_handle) :
                    CmxRef(CmxTypeTagFloat64::cmx_type, immutableComponent, value_handle)
    {
    }
    /// \see CmxRef::CmxRef(int)
    CmxImmutableFloat64(ImmutableComponentPtr immutableComponent) :
                    CmxRef(CmxTypeTagFloat64::cmx_type, immutableComponent)
    {
    }

    typedef CmxTypeInfo<CmxTypeTagFloat64>::c_type c_type;

    // get value in native C++ Type
    operator c_type();
};

/**
 * \brief CMX Immutable Value Reference to BOOL Value
 */
struct CmxImmutableBool : public CmxRef
{
    CmxImmutableBool(ImmutableComponentPtr immutableComponent, int value_handle) :
                    CmxRef(CmxTypeTagBool::cmx_type, immutableComponent, value_handle)
    {
    }
    /// \see CmxRef::CmxRef(int)
    CmxImmutableBool(ImmutableComponentPtr immutableComponent) :
                    CmxRef(CmxTypeTagBool::cmx_type, immutableComponent)
    {
    }

    typedef CmxTypeInfo<CmxTypeTagBool>::c_type c_type;

    // get value in native C++ Type
    operator c_type();
};

/**
 * \brief CMX Immutable Value Reference to STRING Value
 */
struct CmxImmutableString : public CmxRef
{
    CmxImmutableString(ImmutableComponentPtr immutableComponent, int value_handle) :
                    CmxRef(CmxTypeTagString::cmx_type, immutableComponent, value_handle)
    {
    }
    /// \see CmxRef::CmxRef(int)
    CmxImmutableString(ImmutableComponentPtr immutableComponent) :
                    CmxRef(CmxTypeTagString::cmx_type, immutableComponent)
    {
    }

    typedef CmxTypeInfo<CmxTypeTagString>::c_type c_type;

    // get value in native C++ Type
    operator c_type();
};

// ==================== Mutable*

struct MutableComponentMixin
{
    ComponentPtr component_;
    MutableComponentMixin(ComponentPtr component) :
                    component_(component)
    {
    }
};

/**
 * \brief CMX Mutable Value Reference to INT64 Value
 */
struct CmxInt64 : public CmxImmutableInt64, public MutableComponentMixin
{
    CmxInt64(ComponentPtr component, int value_handle) :
                    CmxImmutableInt64(CmxRef::pointerConvert(component), value_handle),
                    MutableComponentMixin(component)
    {
    }
    /// \see CmxRef::CmxRef(int)
    CmxInt64(ComponentPtr component) :
                    CmxImmutableInt64(CmxRef::pointerConvert(component)),
                    MutableComponentMixin(component)
    {
    }

    /// set int64 value
    c_type operator=(const c_type & value);
};

/**
 * \brief CMX Mutable Value Reference to FLOAT64 Value
 */
struct CmxFloat64 : public CmxImmutableFloat64, public MutableComponentMixin
{
    CmxFloat64(ComponentPtr component, int value_handle) :
                    CmxImmutableFloat64(CmxRef::pointerConvert(component), value_handle),
                    MutableComponentMixin(component)
    {
    }
    /// \see CmxRef::CmxRef(int)
    CmxFloat64(ComponentPtr component) :
                    CmxImmutableFloat64(CmxRef::pointerConvert(component)),
                    MutableComponentMixin(component)
    {
    }

    /// set float64 value
    c_type operator=(const c_type & value);
};

/**
 * \brief CMX Mutable Value Reference to BOOL Value
 */
struct CmxBool : public CmxImmutableBool, public MutableComponentMixin
{
    CmxBool(ComponentPtr component, int value_handle) :
                    CmxImmutableBool(CmxRef::pointerConvert(component), value_handle),
                    MutableComponentMixin(component)
    {
    }
    /// \see CmxRef::CmxRef(int)
    CmxBool(ComponentPtr component) :
                    CmxImmutableBool(CmxRef::pointerConvert(component)),
                    MutableComponentMixin(component)
    {
    }

    /// set boolean value
    c_type operator=(const c_type & value);
};

/**
 * \brief CMX Mutable Value Reference to STRING Value
 */
struct CmxString : public CmxImmutableString, public MutableComponentMixin
{
    CmxString(ComponentPtr component, int value_handle) :
                    CmxImmutableString(CmxRef::pointerConvert(component), value_handle),
                    MutableComponentMixin(component)
    {
    }
    /// \see CmxRef::CmxRef(int)
    CmxString(ComponentPtr component) :
                    CmxImmutableString(CmxRef::pointerConvert(component)),
                    MutableComponentMixin(component)
    {
    }

    /// set string value
    c_type operator=(const c_type & value);
};

/**
 * \brief Cast a Cmx Ref from Basic to specific type.
 * \throw CmxTypeCaseException
 */
template<typename _CmxTypeTag>
typename CmxTypeInfo<_CmxTypeTag>::immutable_type cmx_cast(const CmxRef & ref)
{
    if (ref.type() != _CmxTypeTag::cmx_type)
    {
        throw CmxTypeCastException(ref.type_, _CmxTypeTag::cmx_type);
    }
    return typename CmxTypeInfo<_CmxTypeTag>::immutable_type(ref.immutableComponent_, ref.value_handle_);
}

} // namespace cmx
} // namespace cmw
#endif // HAVE_CMW_CMX_TYPES
