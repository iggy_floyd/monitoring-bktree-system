/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_DEMO1
#define HAVE_DEMO1

#include <cmw-cmx-cpp/CmxSupport.h>

namespace cmw {
namespace cmx {
namespace demo {

class Demo1 : cmw::cmx::CmxSupport
{
    cmw::cmx::CmxInt64 counterInt;
    cmw::cmx::CmxFloat64 counterFloat;
    cmw::cmx::CmxBool counterBool;
    cmw::cmx::CmxString counterString;
public:
    Demo1();
    void execute();
};

} // namespace demo
} // namespace cmx
} // namespace cmw
#endif
