/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef DEMO2_H_
#define DEMO2_H_

#include <string>

namespace cmw {
namespace cmx {
namespace demo {

// Forward declaration of class Demo2Metrics
class Demo2Metrics;

class Demo2
{
    Demo2Metrics * metrics;
public:
    Demo2(const std::string & name);
    ~Demo2();
    void execute();
    static void updateProcess();
};

} // namespace demo
} // namespace cmx
} // namespace cmw

#endif /* DEMO2_H_ */
