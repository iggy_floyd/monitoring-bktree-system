/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <cmw-cmx/shm.h>
#include <cmw-cmx/process.h>


const char * value = "n/L866/measure-fail bin/obj-demo/measure-fail.L866.o build/manifest.L866.o -L/acc/local/L866/lib -L/acc/sy"
        "n/L866/measure-fail bin/obj-demo/measure-fail.L866.o build/manifest.L866.o -L/acc/local/L866/lib -L/acc/sy"
        "n/L866/measure-fail bin/obj-demo/measure-fail.L866.o build/manifest.L866.o -L/acc/local/L866/lib -L/acc/sy"
        "n/L866/measure-fail bin/obj-demo/measure-fail.L866.o build/manifest.L866.o -L/acc/local/L866/lib -L/acc/sy"
        "n/L866/measure-fail bin/obj-demo/measure-fail.L866.o build/manifest.L866.o -L/acc/local/L866/lib -L/acc/sy"
        "n/L866/measure-fail bin/obj-demo/measure-fail.L866.o build/manifest.L866.o -L/acc/local/L866/lib -L/acc/sy"
        "n/L866/measure-fail bin/obj-demo/measure-fail.L866.o build/manifest.L866.o -L/acc/local/L866/lib -L/acc/sy";
int main()
{
    cmx_process_update();
    uint64_t time = cmx_common_current_time_usec();
    uint32_t counter = 0;
    cmx_shm * cmx_shm_ptr = NULL;
    int r;
    int value_handle;
    const int len = strlen(value);
    const char * value_2 = value;

    assert(E_CMX_SUCCESS == cmx_shm_create("fast-writer-string", &cmx_shm_ptr));
    value_handle = cmx_shm_add_value_string(cmx_shm_ptr,"TestString", len);
    assert(value_handle > 0);

    while (1)
    {
        if (cmx_common_current_time_usec() - time > 1000000)
        {
            printf("%u = %uK CMX_TYPE_STRING updates/s of len %d\n", counter, counter/1000, len);
            counter = 0;
            time = cmx_common_current_time_usec();
        }
        //
        value_2 = value + (counter % 100);
        r = cmx_shm_set_value_string(cmx_shm_ptr, value_handle, value_2, len - (counter % 100));
        if (E_CMX_SUCCESS != r)
        {
            printf("Write failed: %s", cmx_common_strerror(r));
        }
        else
        {
            counter++;
        }
    }

    return 0;
}
