/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <string.h>
#include <cmw-cmx/cmx.h>
#include <pthread.h>

#define N_WORKER   10
#define N_ROUNDS  200
#define N_METRICS 100

struct data_record
{
    int r[N_METRICS];
};
struct worker_record
{
    struct data_record call[N_ROUNDS];
    struct timespec time[N_ROUNDS];
    pthread_t thread;
};
static struct worker_record worker[N_WORKER];
int cmx_metric[N_METRICS];
static cmx_shm * cmx_shm_ptr;

/* Subtract the `struct timeval' values X and Y,
 storing the result in RESULT. */
void timespec_subtract(struct timespec * result, struct timespec * start, struct timespec * end)
{
    if ((end->tv_nsec - start->tv_nsec) < 0)
    {
        result->tv_sec = end->tv_sec - start->tv_sec - 1;
        result->tv_nsec = 1000000000 + end->tv_nsec - start->tv_nsec;
    }
    else
    {
        result->tv_sec = end->tv_sec - start->tv_sec;
        result->tv_nsec = end->tv_nsec - start->tv_nsec;
    }
}

void * worker_thread(void * threadarg)
{
    size_t worker_n = (size_t) threadarg;
    cmx_shm_value cmx_value;
    cmx_value._int64 = 1234 ^ worker_n;
    struct timespec t1, t2;
    for (int j = 0; j < N_ROUNDS; j++)
    {
        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (int i = 0; i < N_METRICS; i++)
        {
            cmx_value._int64++;
            worker[worker_n].call[j].r[i] = cmx_shm_set_value_single(cmx_shm_ptr, cmx_metric[i], CMX_TYPE_INT64,
                    &cmx_value);
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);
        timespec_subtract(&worker[worker_n].time[j], &t1, &t2);
    }
    return NULL;
}

int main()
{
    cmx_process_update();

    // create shm
    assert(cmx_shm_create("test", &cmx_shm_ptr) == E_CMX_SUCCESS);

    // add metrics
    char name[64];
    for (int i = 0; i < N_METRICS; i++)
    {
        snprintf(name, 64, "m-%d", i);
        cmx_metric[i] = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, name);
        assert(cmx_metric[i] > 0);
    }

    // initialize result data
    for (int k = 0; k < N_WORKER; k++)
    {
        for (int j = 0; j < N_ROUNDS; j++)
        {
            memset(&(worker[k].time[j]), 0, sizeof(struct timespec));
            for (int i = 0; i < N_METRICS; i++)
            {
                worker[k].call[j].r[i] = E_CMX_OPERATION_FAILED;
            }
        }
    }

    for (int i = 0; i < N_WORKER; i++)
    {
        size_t arg = i;
        assert(0==pthread_create(&(worker[i].thread), NULL, worker_thread, (void*) arg));
    }
    for (int i = 0; i < N_WORKER; i++)
    {
        pthread_join(worker[i].thread, NULL);
    }

    assert(E_CMX_SUCCESS == cmx_shm_destroy(cmx_shm_ptr));

    // dump results
    for (int k = 0; k < N_WORKER; k++)
    {
        for (int j = 0; j < N_ROUNDS; j++)
        {
            int ns = (worker[k].time[j].tv_nsec) % 1000;
            int us = (worker[k].time[j].tv_nsec / 1000) % 1000;
            int ms = worker[k].time[j].tv_nsec / 1000000;
            unsigned long ns_sum = worker[k].time[j].tv_nsec
                    + ((unsigned long) worker[k].time[j].tv_sec * 1000000000UL);
            printf("w[%03d],d[%03d],% 2ds % 4dms % 4dus % 4dns, % 8luns\n", //
                    k, j, worker[k].time[j].tv_sec, ms, us, ns, ns_sum);
        }
    }
    return 0;
}
