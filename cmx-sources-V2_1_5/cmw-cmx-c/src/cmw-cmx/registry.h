/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/** \file
 *  \brief Component Registry
 */
#ifndef HAVE_CMW_CMX_C_REGISTRY_H
#define HAVE_CMW_CMX_C_REGISTRY_H
#include <stdint.h>
#include <cmw-cmx/common.h>

typedef struct cmx_component_info_t
{
    char name[CMX_NAME_SZ];
    int32_t process_id;
    char CMX_TAG[8];
} cmx_component_info;

/**
 * \brief Create array with information about all CMX Components
 *
 * The caller is obligated to call free() on the 'components' pointer if returncode
 * is E_CMX_SUCCESS.
 *
 * \param components may be NULL
 * \param len number of cmx components on this machine
 * \return
 *      - ::E_CMX_SUCCESS
 *      - ::E_CMX_INVALID_ARGUMENT
 *      - ::E_CMX_OPERATION_FAILED (access to /dev/shm failed)
 *      - ::E_CMX_OUT_OF_MEMORY (allocation of array failed)
 */
int cmx_registry_discover(cmx_component_info ** const components, int * const len);

/**
 *
 * \param filename_buf   char buffer of size PATH_MAX (at least: 5 + (int_max in _10) + CMX_NAME_SZ)
 * \param process_id
 * \param component_name
 */
void cmx_registry_format_filename(char * filename_buf, const int process_id, const char * const component_name);

/**
 * \param filename_buf
 * \param process_id   not null
 * \param component_name not null (>=CMX_NAME_SZ)
 * \return
 *      - ::CMX_TRUE  parse succeeded
 *      - ::CMX_FALSE parse failed
 */
int cmx_registry_parse_filename(const char * filename_buf, int * const process_id, char * const component_name);

/**
 * \brief Clean up orphaned cmx components.
 *
 * Iterate through the main-registry and delete all components where the process-id is not related to a
 * currently running process.
 * \return
 *      - ::E_CMX_SUCCESS
 *      - see cmx_registry_discover()
 */
int cmx_registry_cleanup(void);

#endif // HAVE_CMW_CMX_C_REGISTRY_H
