#include <boost/python.hpp>

#include <cmw-cmx-cpp/Registry.h>
#include <cmw-cmx-cpp/ImmutableComponent.h>
#include <cmw-cmx-cpp/CmxTypes.h>

using namespace boost::python;

using namespace cmw::cmx;

list Registry_list();

list ImmutableComponent_list(ImmutableComponent * self);

BOOST_PYTHON_MODULE(cmx)
{
    object _Registry = class_<Registry>("Registry") //
    /* Registry.count()   */.def("count", &Registry::count).staticmethod("count") //
    /* Registry.cleanup() */.def("cleanup", &Registry::cleanup).staticmethod("cleanup") //
    /* Registry.list()    */.def("list", &Registry_list).staticmethod("list");

    object _ImmutableComponentPtr = class_<ImmutableComponent, ImmutableComponentPtr>("ImmutableComponentPtr", no_init) //
    /* ImmutableComponentPtr->pid()  */.def("processId", &ImmutableComponent::processId) //
    /* ImmutableComponentPtr->name() */.def("name", &ImmutableComponent::name) //
    /* custom                        */.def("list", &ImmutableComponent_list);

    object _CmxRef = class_<CmxRef>("CmxRef", no_init) //
    /* CmxRef*/.def("name", &CmxRef::name) //
    /* CmxRef*/.def("mtime", &CmxRef::mtime);

    object _CmxImmutableInt64 = class_<CmxImmutableInt64>("CmxImmutableInt64", no_init) //
    /* CmxImmutableInt64 */.def("value", &CmxImmutableInt64::operator CmxImmutableInt64::c_type)
    /* CmxImmutableInt64 */.def("mtime", &CmxImmutableInt64::mtime)
    /* CmxImmutableInt64 */.def("name", &CmxImmutableInt64::name);

    object _CmxImmutableFloat64 = class_<CmxImmutableFloat64>("CmxImmutableFloat64", no_init) //
    /* CmxImmutableFloat64 */.def("value", &CmxImmutableFloat64::operator CmxImmutableFloat64::c_type)
    /* CmxImmutableFloat64 */.def("mtime", &CmxImmutableFloat64::mtime)
    /* CmxImmutableFloat64 */.def("name", &CmxImmutableFloat64::name);

    object _CmxImmutableBool = class_<CmxImmutableBool>("CmxImmutableInt64", no_init) //
    /* CmxImmutableBool */.def("value", &CmxImmutableBool::operator CmxImmutableBool::c_type)
    /* CmxImmutableBool */.def("mtime", &CmxImmutableBool::mtime)
    /* CmxImmutableBool */.def("name", &CmxImmutableBool::name);

    object _CmxImmutableString = class_<CmxImmutableString>("CmxImmutableString", no_init) //
    /* CmxImmutableString */.def("value", &CmxImmutableString::operator CmxImmutableString::c_type)
    /* CmxImmutableString */.def("mtime", &CmxImmutableString::mtime)
    /* CmxImmutableString */.def("name", &CmxImmutableString::name);
}
