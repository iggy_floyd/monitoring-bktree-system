
/*
 * @2014 Igor Marfin <Unister Gmbh> igor.marfin@unister.de
 * 
 * An application which sets up some CMX component in the Registry.
 * The value of this component can be read out later. 
 * 
 * 
*/

#include <iostream>
#include <cmw-cmx-cpp/Log.h>
#include <cmw-cmx-cpp/ProcessComponent.h>
#include <cmw-cmx-cpp/Registry.h>
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/CmxSupport.h>
#include <exception>
#include <unistd.h>
#include <cxxtools/arg.h>


static void cmxadm_help()
{
    std::cout << ("Usage:\n"
            "  --nameComponent  Register a component with \'nameComponent\'\n"
            "  --nameValue  Set the name \'nameValue\' of the value of the component \'nameComponent\'\n"
            "  --value  Set the value of the  \'nameValue\'\n"
            "  --wait  in microseconds \n"
          );
}

static cmw::cmx::ComponentPtr component;



int main(int argc, char* argv[])
{

    try {
        // read a cmd line
        cxxtools::Arg<std::string> nameComponent(argc, argv, "--nameComponent", "TestComponent");
        cxxtools::Arg<std::string> nameValue(argc, argv, "--nameValue", "TestValue");
        cxxtools::Arg<std::string> value(argc, argv, "--value", "TestString");
        cxxtools::Arg<unsigned int> wait(argc, argv, "--wait", 1000000);
        cxxtools::Arg<bool> help(argc, argv, '?');
        if (help)
        {
            cmxadm_help();
            return 0;
        }

        cmw::cmx::ProcessComponent::update();
        std::cout<<component<<std::endl;
        component = cmw::cmx::Component::create(nameComponent.getValue());
        cmw::cmx::CmxString valueComponent = component->newString(nameValue.getValue(),256);
        valueComponent = value.getValue();
        usleep(wait.getValue());
        cmw::cmx::ProcessComponent::update();        
    } catch (const std::exception& e) {
        std::cout<<"failed\n";
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}


/**

how-to compile:
@compile@ g++ -c cmx_string_setter.cpp -o cmx_string_setter.o -Wno-long-long -Wall -pedantic  -g -O2 -I. -I../cxxtools/include  -rdynamic -fnon-call-exceptions  -I ../include/ -std=c++11 -I ../src  -I../cmx-sources-V2_1_5/cmw-cmx-c/target/include -I../cmx-sources-V2_1_5/cmw-cmx-cpp/target/include

how-to link: 
@link@	  /bin/bash ../cxxtools//libtool  --tag=CXX   --mode=link g++  -g -O2    -o cmx_string_setter cmx_string_setter.o ../cxxtools/src/libcxxtools.la -ldl -lrt -lnsl ../cmx-sources-V2_1_5/cmw-cmx-cpp/target/lib/libcmw-cmx-cpp.a ../cmx-sources-V2_1_5/cmw-cmx-c/target/lib/libcmw-cmx.a

how-to run:
@run@	./cmx_string_setter
 
how-to test:
@test@  ./cmx_string_setter --wait 10000000 & sleep 5; echo "test"; ../cmx-sources-V2_1_5/cmw-cmx-tools-cpp/target/bin/cmw-cmx-reader -n TestComponent


**/

