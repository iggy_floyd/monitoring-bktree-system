
/*

    A test of the LockFile class

*/

#include <iostream>
#include <boost/thread.hpp>

#include "backuper.hpp"
#include "Monitor.hpp"



#define LOCKWRITEFILE "/home/debian/debian-package/netbeans/monitoring-bktree-system/dump/.writelock"
#define LOCKREADFILE  "/home/debian/debian-package/netbeans/monitoring-bktree-system/dump/.readlock"


bool read_reply = true;
int main(int argc, char* argv[])
{
    try {
        Monitor::initialize("BKTREE_MONITOR");
        int nMonitorings = 20;
        while((--nMonitorings)>0) {
        
    // example  1: to save several dumps
    struct example1 {
        static void test () {
        std::cout<<"\n\nTest1: save several dumps\n\n";
        //save dump1
        std::cout<<"(1)test if a  backup system is ready...\n";
        tools::backup::helper::LockFile lockfile_write(LOCKWRITEFILE,false); // false --> no autoclean in destructor (not to remove lock files created by an external process)
        if (!lockfile_write.try_lock()) return; // if the backup system is in progress?
        lockfile_write.unlock(false);  // false --> not to remove lock files created by an external process
        std::cout<<"(1)backup system is ready...\n";
        {
           tools::backup::helper::LockFile lockfile_read(LOCKREADFILE,true);
           lockfile_read.lock(); //wait until the previous dump saving has finished
            std::cout<<"(1)saving dumps ...\n";
            boost::this_thread::sleep(boost::posix_time::seconds(1)); // save dumps
            std::cout<<"(1)dumps ...saved\n";
        }
        std::cout<<"(1)send a request to the Backupsystem...\n";
        tools::backup::BackupCommunicator::makeRequest(tools::backup::kBackup,true); // true --> use internal delay of about 40 ms
                                                                                     //we do not care about returned status


        std::cout<<"(1)a request to the Backupsystem...sent\n";

        //save dump2
        std::cout<<"(2)test if a  backup system is ready...\n";
        if (!lockfile_write.try_lock()) return; //if the backup system is in progress?
        lockfile_write.unlock(false); // false --> not to remove lock files created by an external process
        std::cout<<"(2)backup system is ready...\n";
        {
           tools::backup::helper::LockFile lockfile_read(LOCKREADFILE,true);
           lockfile_read.lock(); //wait until the previous dump saving has finished
            std::cout<<"(2)saving dumps ...\n";
            boost::this_thread::sleep(boost::posix_time::seconds(1)); // save dumps
            std::cout<<"(2)dumps ...saved\n";
        }
        std::cout<<"(2)send a request to the Backupsystem...\n";
        tools::backup::BackupCommunicator::makeRequest(tools::backup::kBackup,true); // true -> use internal delay of about 40 ms
                                                                                     //we do not care about returned status

        std::cout<<"(2)a request to the Backupsystem...sent\n";
        }

    };
    Monitor _monitor("Example1",std::string("example1"));
    example1::test();
    {Monitor __monitor("PASSED",std::string("example1"));}
    


    // example 2: to load several dumps from the backup
    struct example2 {
        static void test() {
                std::cout<<"\n\nTest2: load several dumps\n\n";
                // restore the dump 1
                int status=1; //an 'error' status by default
                std::cout<<"(3)test if a  backup system is ready...\n";
                tools::backup::helper::LockFile lockfile_write(LOCKWRITEFILE,false);   // false --> no autoclean in destructor (not to remove lock files created by an external process)
                lockfile_write.lock(); // if the backup system is in progress, wait
                std::cout<<"(3)backup system is ready...\n";
                // now the backup system is in the idle state
                usleep(5000); // wait 5 ms until netcat has started listing a port.
                std::cout<<"(3)send a request to the Backupsystem...\n";
                if (tools::backup::BackupCommunicator::makeRequest(tools::backup::kRestore)) {
                    status = tools::backup::BackupCommunicator::getStatus(); // will wait until backup system doesn't restore the dumps and sends a status to us
                }
                std::cout<<"(3)a request to the Backupsystem...sent\n";
                lockfile_write.unlock(true);   // true --> to remove lock files created by an external process


                std::cout<<"(3)status of the command "<<status<<std::endl;
                std::stringstream ss;
                ss.clear();
                ss<<status<<std::endl;

                if (status >0 ) return; // if error was happening, return
                {
                    tools::backup::helper::LockFile lockfile_read(LOCKREADFILE,true);
                    lockfile_read.lock(); //wait until the previous dump loading  has finished
                    std::cout<<"(3)loading dumps ...\n";
                    boost::this_thread::sleep(boost::posix_time::seconds(1)); // load dumps
                    std::cout<<"(3)dumps ...loaded\n";
                }



                // restore the dump 2
                status=1; //an 'error' status by default
                std::cout<<"(4)test if a  backup system is ready...\n";
                lockfile_write.lock(); // if the backup system is in progress, wait
                std::cout<<"(4)backup system is ready...\n";
                // now the backup system is in the idle stat
                usleep(5000); // wait 5 ms until netcat has started listing a port.
                std::cout<<"(4)send a request to the Backupsystem...\n";
                if (tools::backup::BackupCommunicator::makeRequest(tools::backup::kRestore)) {
                    status = tools::backup::BackupCommunicator::getStatus(); // will wait until backup system doesn't restore the dumps and sends a status to us
                }
                std::cout<<"(4)a request to the Backupsystem...sent\n";
                lockfile_write.unlock(true);
                std::cout<<"(4)status of the command "<<status<<std::endl;

                ss.clear();
                ss<<status<<std::endl;

                if (status >0 ) return; // if error was happening, return
                {
                    tools::backup::helper::LockFile lockfile_read(LOCKREADFILE,true);
                    lockfile_read.lock(); //wait until the previous dump loading  has finished
                    std::cout<<"(4)loading dumps ...\n";
                    boost::this_thread::sleep(boost::posix_time::seconds(1)); // load dumps
                    std::cout<<"(4)dumps ...loaded\n";
                }


        }

    };
    Monitor _monitor2("Example2",std::string("example2"));
    example2::test();
    {Monitor __monitor("PASSED",std::string("example2"));}


    /* example 3: realizes the following scenario
     *            1)  load the latest backup
     *            2)  loading is crashed
     *            3)  load the second backup and loading is crashed again
     *            4)  load the third  backup and loading is crashed again
     *
    */
    struct example3 {
            static void test() {
                std::cout<<"\n\nTest3: load and crashed several dumps\n\n";


                int num_crashed=3;
                while (--num_crashed>0) {
                    int status=1; //an 'error' status by default
                    std::stringstream ss;
                    ss.clear();
                    try {

                        std::cout<<"attempts to crash the system "<<num_crashed<<"\n";

                        ss<<num_crashed;
                        std::cout<<"(5)test if a  backup system is ready...\n";
                        tools::backup::helper::LockFile lockfile_write(LOCKWRITEFILE,false);   // false --> no autoclean in destructor (not to remove lock files created by an external process)
                        lockfile_write.lock(); // if the backup system is in progress, wait
                        std::cout<<"(5)backup system is ready...\n";
                        // now the backup system is in the idle state
                        usleep(5000); // wait 5 ms until netcat has started listing a port.
                        std::cout<<"(5)send a request to the Backupsystem...\n";
                        auto cmd = (num_crashed == 2)? tools::backup::kRestore:tools::backup::kLoadCrashed;
                        if (tools::backup::BackupCommunicator::makeRequest(cmd)) {
                            status = tools::backup::BackupCommunicator::getStatus(); // will wait until backup system doesn't restore the dumps and sends a status to us
                        }
                        std::cout<<"(5)a request to the Backupsystem...sent\n";
                        lockfile_write.unlock(true);   // true --> to remove lock files created by an external process

                        std::cout<<"(5)status of the command "<<status<<std::endl;
                        ss.clear();
                        ss<<status<<std::endl;

                        if (status >0 ) continue; // if error was happening, continue
                        {
                            tools::backup::helper::LockFile lockfile_read(LOCKREADFILE,true);
                            lockfile_read.lock(); //wait until the previous dump loading  has finished
                            std::cout<<"(5)loading dumps ...\n";
                            boost::this_thread::sleep(boost::posix_time::seconds(1)); // load dumps
                            // crash situation happens here
                            throw std::runtime_error("[DUMP_LOAD_CRASHED]");
                            std::cout<<"(5)dumps ...loaded\n";
                        }
                    } catch (std::exception &e) {

                        // send the LOAD_CRASHED request to backup-system

                        std::cout<<"(6)test if a  backup system is ready...\n";
                        tools::backup::helper::LockFile lockfile_write(LOCKWRITEFILE,false);   // false --> no autoclean in destructor (not to remove lock files created by an external process)
                        lockfile_write.lock(); // if the backup system is in progress, wait
                        std::cout<<"(6)backup system is ready...\n";
                        usleep(5000); // wait 5 ms until netcat has started listing a port.
                        std::cout<<"(6)send a request to the Backupsystem...\n";
                        status=1;
                        if (tools::backup::BackupCommunicator::makeRequest(tools::backup::kLoadCrashed)) {
                            status = tools::backup::BackupCommunicator::getStatus(); // will wait until backup system doesn't restore the dumps and sends a status to us
                        }
                        std::cout<<"(6)a request to the Backupsystem...sent\n";
                        lockfile_write.unlock(true);   // true --> to remove lock files created by an external process

                        std::cout<<"(6)status of the command "<<status<<std::endl;
                        ss.clear();
                        ss<<status<<std::endl;
                        if (status >0 ) continue; // if error was happening, continue
                        {
                            tools::backup::helper::LockFile lockfile_read(LOCKREADFILE,true);
                            lockfile_read.lock(); //wait until the previous dump loading  has finished
                            std::cout<<"(6)restoring crashed dumps ...\n";
                            boost::this_thread::sleep(boost::posix_time::seconds(1)); // load dumps
                            // crash situation happens here
                            std::cout<<"(6)crashed dumps ...loaded\n";
                        }
                    }
                 }

            }

    };
    Monitor _monitor3("Example3",std::string("example3"));
    example3::test();
    {Monitor __monitor("PASSED",std::string("example3"));}
        }

    } catch (const std::exception& e) {
        std::cout<<"failed\n";
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}


/**

compilation:
@compile@ g++ -c lockfile_monitor.cpp -o lockfile_monitor.o -Wno-long-long -Wall -pedantic  -g -O2 -I. -I../cxxtools/include  -rdynamic -fnon-call-exceptions  -std=c++11  `( cd ..; ./config.sh --inc )`
@link@	  /bin/bash ../cxxtools//libtool  --tag=CXX   --mode=link g++  -g -O2    -o lockfile_monitor lockfile_monitor.o ../cxxtools/src/libcxxtools.la -ldl -lrt -lnsl  -lpthread -lboost_thread-mt `( cd ..; ./config.sh --libso )`



how-to run
@run@	./lockfile_monitor


**/

