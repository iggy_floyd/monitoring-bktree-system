# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project




all: configuration doc README.wiki build link test 

# to clean all temporary stuff
scons:=$(shell echo `pwd`)/scons-build/bin/scons

# to check that the system has all needed components
configuration: configure
	@./configure
	@ cd cxxtools; ./configure; make


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac



doc: README.wiki
	-@echo "\n\nDoc building...\n\n"
	-@ mkdir doc
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	-@ rm *info
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "cp {}/index.html doc; rm -r {}" | sh


test: 	build link
	-@echo "\n\nTesting... \n\n"
	-@for dir in `find ./  -maxdepth 1 -type d | grep -v cxxtools | awk '{if(NR>1) print $0}'`; do echo $$dir; cd $$dir; find ./ -iname "*.cpp" | xargs -I {}  echo "grep @test@ {} | sed 's/@test@//' | sh " | sh;  cd - 2> /dev/null; done		



build:
	-@echo "\n\nBuilding...\n\n"
	-@for dir in `find cmx-sources-V2_1_5/  -maxdepth 1 -type d | sort`; do echo $(dir); cd $$dir;  $(scons)  ; cd - 2> /dev/null; done
	-@for dir in `find ./  -maxdepth 1 -type d | grep -v cxxtools | awk '{if(NR>1) print $0}'`; do echo $$dir; cd $$dir; find ./ -iname "*.cpp" | xargs -I {}  echo "grep @compile@ {} | sed 's/@compile@//' | sh " | sh;  cd - 2> /dev/null; done
	

link:
	-@echo "\n\nLinking...\n\n"
	-@for dir in `find ./  -maxdepth 1 -type d | grep -v cxxtools | awk '{if(NR>1) print $0}'`; do echo $$dir; cd $$dir; find ./ -iname "*.cpp" | xargs -I {}  echo "grep @link@ {} | sed 's/@link@//' | sh " | sh;  cd - 2> /dev/null; done	
 


run:  build link
	-@echo "\n\nRunning...\n\n"
	-@for dir in `find ./  -maxdepth 1 -type d | grep -v cxxtools | awk '{if(NR>1) print $0}'`; do echo $$dir; cd $$dir; find ./ -iname "*.cpp" | xargs -I {}  echo "grep @run@ {} | sed 's/@run@//' | sh " | sh;  cd - 2> /dev/null; done		


# to clean all temporary stuff
clean: 
	-@echo "\n\nCleaning...\n\n"
	-@cd cxxtools;  make clean;
	-@rm test/*.o
	-@rm lib/{*.a,*.so}
	-@rm src/*.o
	-@rm -r test/.libs
	-@find test/ -executable -type f  |grep -v test.py | xargs -I {} rm -r {}
	-@for dir in `find cmx-sources-V2_1_5/  -maxdepth 1 -type d`; do echo $(dir); cd $$dir;  $(scons) -c ; cd - 2> /dev/null; done
	-@find cmx-sources-V2_1_5/  -iname "*.pyc" |  xargs -I {} rm -r {}
	-@find scons-build/  -iname "*.pyc" |  xargs -I {} rm -r {}



.PHONY: configuration clean all doc run test build start_daemon link
