export SCONSPATH=`pwd`/scons-build
export PYTHONPATH=$SCONSPATH/lib/python2.7/site-packages/
alias scons=$SCONSPATH/bin/scons
make configuration # optional
make build
make link
